## Introduction
For provisioning machines, it is better to use an IT automation tool. Provisioning with Ansible allows you to 
seamlessly transition into configuration management, orchestration and application deployment using the same simple, 
human readable, automation language.
But to make a long story short, ansible does not work on a Windows control machine, so the Vagrantfile contains a separate control vm
where ansible is installed via shell provisioner.


## Clone the repository
To get starterd, start a shell (for example CMDER) and go to a folder where you want your projects to live.

```
cd <project folder>
git clone https://bitbucket.org/zuyd_services/assignment2.git
cd assignment2
```

## Start vagrant
Start vagrant from the project folder with the command 'vagrant up'. By starting the vagrant instances new virtual machines will be created, and the webserver will be accessible on [http://172.16.1.203].
Please check the [vagrant CLI manual](https://www.vagrantup.com/docs/cli/) for more options, such as vagrant destroy and vagrant ssh.

```
vagrant up

vagrant ssh

vagrant destroy
```

### Credentials
For login in into the machines, vagrant ssh can be used. There is no password needed, since Vagrant already installed the keys.

For joining the Domain the following credentials can be used.
```
smb_username : 'administrator'
smb_password : 'Password123'
```

### Windows
To join the windows machine to the Domain, be aware to verify and change the Host-Only to the correct network.